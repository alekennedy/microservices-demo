package com.microservices.demo.common.config;

import com.microservices.demo.twitter.to.kafka.service.config.RetryConfigData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialRandomBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@ComponentScan(basePackages = "com.microservices.demo")
public class RetryConfig {
    private RetryConfigData retryConfigData;

    public RetryConfig(RetryConfigData configData){
        this.retryConfigData = configData;
    }

    @Bean
    public RetryTemplate retryTemplate(){
        RetryTemplate retryTemplate = new RetryTemplate();
        ExponentialRandomBackOffPolicy exponentialRandomBackOffPolicy = new ExponentialRandomBackOffPolicy();
        exponentialRandomBackOffPolicy.setInitialInterval(retryConfigData.getInitialIntervalMs());
        exponentialRandomBackOffPolicy.setMaxInterval(retryConfigData.getMaxIntervalMs());
        exponentialRandomBackOffPolicy.setMultiplier(retryConfigData.getMultiplier());

        retryTemplate.setBackOffPolicy(exponentialRandomBackOffPolicy);

        SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
        simpleRetryPolicy.setMaxAttempts(retryConfigData.getMaxAttempts());
        retryTemplate.setRetryPolicy(simpleRetryPolicy);


        return retryTemplate;
    }


}
